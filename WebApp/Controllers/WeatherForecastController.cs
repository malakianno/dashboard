﻿using System;
using System.Threading.Tasks;
using Logic.Models;
using Logic.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly IWeatherService _weatherService;
        private readonly IVisitorsHistoryService _userDataService;
        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(IWeatherService weatherService, IVisitorsHistoryService userDataService, ILogger<WeatherForecastController> logger)
        {
            _weatherService = weatherService;
            _userDataService = userDataService;
            _logger = logger;
        }

        [HttpGet]
        [Route("hourly")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetHourlyForecast(double latitude, double longitude)
        {
            try
            {
                var result = await _weatherService.GetHourlyWeatherForecast(latitude, longitude);
                var userData = new VisitorInfo()
                {
                    Timestamp = DateTime.UtcNow,
                    Latitude = latitude,
                    Longitude = longitude,
                    City = result.City,
                    IpAddress = Request.HttpContext.Connection.RemoteIpAddress?.ToString()
                };
                _userDataService.Add(userData);
                return Ok(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return StatusCode(500);
            }
        }
    }
}
