using System;
using System.Linq;
using Logic.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VisitorsHistoryController : ControllerBase
    {
        private readonly IVisitorsHistoryService _userDataService;
        private readonly ILogger<VisitorsHistoryController> _logger;
        
        public VisitorsHistoryController(IVisitorsHistoryService userDataService, ILogger<VisitorsHistoryController> logger)
        {
            _userDataService = userDataService;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Get()
        {
            try
            {
                var usersData = _userDataService.Get().OrderByDescending(x => x.Timestamp);
                return Ok(usersData);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return StatusCode(500);
            }
        }
    }
}