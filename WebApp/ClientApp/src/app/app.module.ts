import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { WeatherForecastComponent } from './weather-forecast/weather-forecast.component';
import { WeatherForecastChartComponent } from './weather-forecast-chart/weather-forecast-chart.component'
import { baseUrl, baseUrlToken } from './app.providers'
import { routing } from './app.routing'
import { ErrorHandlerService } from './shared/error-handler.service';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    WeatherForecastComponent,
    WeatherForecastChartComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    routing
  ],
  providers: [
    { provide: baseUrlToken, useValue: baseUrl },
    { provide: ErrorHandler, useClass: ErrorHandlerService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
