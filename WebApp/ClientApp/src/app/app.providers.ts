import { InjectionToken } from '@angular/core'

export const baseUrlToken = new InjectionToken('BASE_URL');

export const baseUrl = getBaseUrl();

function getBaseUrl() {
  return document.getElementsByTagName('base')[0]?.href + 'api/';
}
