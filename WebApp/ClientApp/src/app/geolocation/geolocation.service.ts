import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'

@Injectable({
    providedIn: 'root'
})

export class GeolocationService {
    private readonly defaultLatitude: number = 0;
    private readonly defaultLongitude: number = 0;
    
    getLocation(timeout: number = 10000): Observable<Coordinates> {
        const defaultCoordinates = new Coordinates(this.defaultLatitude, this.defaultLongitude);
        return new Observable<Coordinates>(subscriber  => {
          if (window.navigator && window.navigator.geolocation) {
            let errorOccurred = false;
            
            navigator.geolocation.getCurrentPosition(
              // Success callback
              (coordinates: Position) => {
                const {latitude, longitude } = coordinates.coords;
                subscriber.next(new Coordinates(latitude, longitude));
                subscriber.complete();
              },
              // Error callback
              (error: PositionError) => {
                errorOccurred = true;
                console.warn(error.code, error.message);
        
                if (error.code === 1) {
                  // the user said no
                } else if (error.code === 2) {
                  // position unavailable
                } else if (error.code === 3) {
                  // timeout
                }
                subscriber.next(defaultCoordinates);
                subscriber.complete();
              },
              // Options
              {
                enableHighAccuracy: true,
                timeout: timeout,
                maximumAge: 0
              }
            );
            setTimeout(() => {
              // This workaround is necessary to return the default coordinates because in some
              // cases 'getCurrentPosition()' doesn't return anything and doesn't throw any exceptions
              if(!subscriber.closed && !errorOccurred) {
                subscriber.next(defaultCoordinates);
                subscriber.complete();
              }
            }, timeout);
          } else {
            console.warn("Geolocation is not available");
            subscriber.next(defaultCoordinates);
            subscriber.complete();
          }
        });
    }
}

export class Coordinates {
    latitude: number
    longitude: number

    constructor(latitude: number, longitude: number ) {
      this.latitude = latitude;
      this.longitude = longitude;
    }
}