import { TestBed, async, inject } from '@angular/core/testing'
import { GeolocationService } from './geolocation.service';

describe('GeolocationService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ GeolocationService ]
        })
    });

    it('should return value even without interaction with the user', async (inject([GeolocationService], (service: GeolocationService) => {
        service.getLocation(100).subscribe((coordinates) => {
            expect(coordinates).toBeDefined();
            expect(coordinates.latitude).toEqual(jasmine.any(Number));
            expect(coordinates.longitude).toEqual(jasmine.any(Number));
        });
    })))
});