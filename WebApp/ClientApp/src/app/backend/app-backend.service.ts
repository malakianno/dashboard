import { Inject, Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { baseUrlToken } from '../app.providers'
import { WeatherForecast } from './app.models'
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BackendService {
    constructor(
        private http: HttpClient,
        @Inject(baseUrlToken) private baseUrl: string
    ) {
    }
    public getWeatherForecast(latitude: number, longitude: number): Observable<WeatherForecast> {
        const getOptions = {
            params: {
                latitude: latitude.toString(),
                longitude: longitude.toString()
            }
        };
        return this.http.get<WeatherForecast>(`${this.baseUrl}weatherforecast/hourly`, getOptions);
    }
}