export interface WeatherForecast {
    city: string;
    items: WeatherForecastItem[];
  }
  
  export interface WeatherForecastItem {
    time: Date;
    timeFormatted: string,
    timeSecondsSince1970: number;
    temperatureF: number;
    maxTemperatureF: number;
    minTemperatureF: number;
    temperatureC: number;
    minTemperatureC: number;
    maxTemperatureC: number;
    wind: number;
    description: string;
  }