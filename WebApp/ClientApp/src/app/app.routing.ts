import { RouterModule, Route } from '@angular/router'
import { WeatherForecastChartComponent } from './weather-forecast-chart/weather-forecast-chart.component'
import { WeatherForecastComponent } from './weather-forecast/weather-forecast.component'

const appRoutes: Route[] = [
  { path: 'weather-forecast', component: WeatherForecastComponent },
  { path: 'weather-forecast-chart', component: WeatherForecastChartComponent },
  { path: 'statistic', loadChildren: () => import('./statistic/statistic.module').then(x => x.StatisticModule) },
  { path: '', redirectTo: 'weather-forecast-chart', pathMatch: 'full' }
];

export const routing = RouterModule.forRoot(appRoutes);
