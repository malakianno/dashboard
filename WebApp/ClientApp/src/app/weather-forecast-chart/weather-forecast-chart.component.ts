import { Component, OnInit, Inject } from '@angular/core'
import * as Highcharts from 'highcharts'
import { ChartBuilder } from './chart-builder.service'
import { BackendService } from '../backend/app-backend.service'
import { GeolocationService } from '../geolocation/geolocation.service';

@Component({
  selector: 'weather-forecast-chart',
  templateUrl: './weather-forecast-chart.template.html',
  styleUrls: ['./weather-forecast-chart.css']
})

export class WeatherForecastChartComponent implements OnInit {
  private readonly container: string = 'highcharts-container';

  constructor(
    private chartBuilder: ChartBuilder,
    private backendService: BackendService,
    private geolocationService: GeolocationService
  ) {
  }

  ngOnInit() {
    this.geolocationService.getLocation().subscribe(coordinates => {
      this.backendService.getWeatherForecast(coordinates.latitude, coordinates.longitude)
      .subscribe(response => {
        let options = this.chartBuilder.getOptions();
        var series = response.items.map(item => [item.timeSecondsSince1970 * 1000, item.minTemperatureF]);
        this.chartBuilder.addSeries(options, response.city, series);
        Highcharts.chart(this.container, options);
      });
    });
  }
}
