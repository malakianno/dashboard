import { Injectable } from '@angular/core'
import * as Highcharts from 'highcharts'

@Injectable({
  providedIn: 'root'
})

export class ChartBuilder {
  getOptions(): Highcharts.Options {
    let options: Highcharts.Options = {
      chart: {
        type: 'area'
      },
      title: {
        text: 'Weather Forecast'
      },
      xAxis: {
        type: 'datetime',
        tickInterval: 1000 * 60 * 60 * 6 // 6 hours
      },
      yAxis: {
        title: {
          text: 'Temperature (F)'
        }
      },
      series: []
    };
    return Highcharts.setOptions(options);
  }

  addSeries(options: Highcharts.Options, name: string, data: number[][]) {
    options.series.push({
        name: name,
        data: data.map(record => {
          var point = {
            x: record[0],
            y: record[1]
          } as Highcharts.PointOptionsObject;
          return point;
        })
      } as Highcharts.SeriesOptionsType
    );
  }
}
