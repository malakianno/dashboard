import { Component, OnInit } from '@angular/core';
import { WeatherForecast } from '../backend/app.models'
import { BackendService } from '../backend/app-backend.service'
import { GeolocationService } from '../geolocation/geolocation.service'

@Component({
  selector: 'weather-forecast',
  templateUrl: './weather-forecast.component.html'
})
export class WeatherForecastComponent implements OnInit {
  public forecast: WeatherForecast;

  constructor(private backendService: BackendService,
    private geolocationService: GeolocationService) {
  }

  ngOnInit() {
    this.geolocationService.getLocation().subscribe(coordinates => {
      this.getWeatherForecast(coordinates.latitude, coordinates.longitude);
    });
  }

  private getWeatherForecast(latitude: number, longitude: number) {
    this.backendService.getWeatherForecast(latitude, longitude).subscribe(result => {
      this.forecast = result;
    }, error => console.error(error));
  }
}
