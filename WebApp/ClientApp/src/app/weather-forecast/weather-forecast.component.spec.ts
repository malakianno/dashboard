import { TestBed, ComponentFixture } from '@angular/core/testing' 
import { BrowserModule, By } from '@angular/platform-browser';
import { ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { WeatherForecastComponent } from '../weather-forecast/weather-forecast.component';
import { routing } from '../app.routing'
import { ErrorHandlerService } from '../shared/error-handler.service';
import { WeatherForecast, WeatherForecastItem } from '../backend/app.models'
import { BackendService } from '../backend/app-backend.service';
import { Observable } from 'rxjs';
import { APP_BASE_HREF } from '@angular/common';
import { GeolocationService } from '../geolocation/geolocation.service';

describe('WeatherForecastComponent', () => {
    let component: ComponentFixture<WeatherForecastComponent>;

    beforeEach(() => {
        let geolocationService = jasmine.createSpyObj<GeolocationService>('GeolocationService', {
            getLocation: new Observable<Coordinates>((subscribe) => {
                subscribe.next({latitude: 0, longitude: 0} as Coordinates);
                subscribe.complete();
            })
        });

        let backendService = jasmine.createSpyObj<BackendService>('BackendService', {
            getWeatherForecast: new Observable<WeatherForecast>((subscribe) => {
                subscribe.next({city: 'TestCity', items: [{time: new Date(), temperatureF: 75 } as WeatherForecastItem]} as WeatherForecast);
                subscribe.complete();
            })
        });
        
        TestBed.configureTestingModule({
            declarations: [
                WeatherForecastComponent,
              ],
              imports: [
                BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
                HttpClientModule,
                FormsModule,
                routing
              ],
              providers: [
                { provide: APP_BASE_HREF, useValue: '/'},
                { provide: ErrorHandler, useClass: ErrorHandlerService },
                { provide: BackendService, useValue: backendService },
                { provide: GeolocationService, useValue: geolocationService }
              ]
        });

        component = TestBed.createComponent(WeatherForecastComponent);
    });

    it('should display a weather forecast table', () => {
        component.detectChanges();
        let element = component.debugElement.query(By.css('table'));
        let html = element.nativeElement;

        expect(html).toBeTruthy();
    })

    it('should have 1 record in the weather forecast table', () => {
        component.detectChanges();
        let recordsCount = component.debugElement.queryAll(By.css('table > tbody > tr')).length;

        expect(recordsCount).toBe(1)
    })
})