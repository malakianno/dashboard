import { Injectable, ErrorHandler } from '@angular/core'

@Injectable({
    providedIn: 'root'
})
export class ErrorHandlerService extends ErrorHandler {
    constructor() {
        super();
    }

    handleError(error) {
        var date = new Date();
        console.error({
            timestamp: date.toISOString(),
            message: error.message,
            zone: error.zone,
            task: error.task
        });
    }
}