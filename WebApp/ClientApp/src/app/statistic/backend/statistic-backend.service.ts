import { HttpClient } from "@angular/common/http";
import { Injectable, Inject } from "@angular/core";
import { VisitorInfo } from "./statistic.models";
import { Observable } from "rxjs";
import { baseUrlToken } from "src/app/app.providers";

@Injectable()
export class StatisticBackendService{
    constructor(
        @Inject(baseUrlToken) private baseUrl: string,
        private http: HttpClient) {       
    }

    public getVisitors(): Observable<VisitorInfo[]> {
        return this.http.get<VisitorInfo[]>(`${this.baseUrl}visitorshistory`);
    }
}