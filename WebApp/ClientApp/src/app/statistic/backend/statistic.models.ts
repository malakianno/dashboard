export interface VisitorInfo {
    timestamp: Date;
    ipAddress: string;
    latitude: number;
    longitude: number;
    city: string;
}