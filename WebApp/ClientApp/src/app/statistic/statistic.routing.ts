import { Route, RouterModule } from '@angular/router'
import { VisitorsHistoryComponent } from './visitors-history/visitors-history.component'

const statisticRoutes: Route[] = [
    { path: 'visitors-history', component: VisitorsHistoryComponent }
]

export const statisticRouting = RouterModule.forChild(statisticRoutes);