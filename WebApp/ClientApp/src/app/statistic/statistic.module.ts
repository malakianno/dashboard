import { NgModule } from '@angular/core'
import { VisitorsHistoryComponent } from './visitors-history/visitors-history.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { statisticRouting } from './statistic.routing';
import { StatisticBackendService } from './backend/statistic-backend.service';
import { VisitorCitiesPipe } from './shared/visitor-cities.pipe';
import { LocationDirective } from './shared/location.directive';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        statisticRouting
    ],
    providers: [
        StatisticBackendService
    ],
    declarations: [
        VisitorsHistoryComponent,
        LocationDirective,
        VisitorCitiesPipe
    ]
})
export class StatisticModule {}