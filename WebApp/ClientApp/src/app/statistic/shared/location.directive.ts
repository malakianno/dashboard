import { Directive, HostBinding, Input } from "@angular/core";

@Directive({selector: '[dsLocation]'})
export class LocationDirective {boolean
    @HostBinding('class.default-location') isDefaultLocation = false;
    private readonly defaultLocation: string = "Globe";
    
    @Input() set dsLocation(value) {
        if(value === this.defaultLocation) {
            this.isDefaultLocation = true;
        }
    }
}