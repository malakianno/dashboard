import { Pipe, PipeTransform } from "@angular/core";
import { VisitorInfo } from "../backend/statistic.models";

@Pipe({name: 'dsVisitorCities'})
export class VisitorCitiesPipe implements PipeTransform {
    transform(value: VisitorInfo[]) {
        let result: string[] = [];
        value.forEach(x => {
            if(result.indexOf(x.city) <= -1) {
                result.push(x.city);
            }
        });
        return result.join(", ");
    }
}