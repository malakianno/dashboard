import { Component, OnInit } from '@angular/core'
import { VisitorInfo } from '../backend/statistic.models';
import { StatisticBackendService } from '../backend/statistic-backend.service';

@Component({
    selector: 'visitors-history',
    templateUrl: 'visitors-history.component.html',
    styleUrls: ['visitors-history.component.css']
})
export class VisitorsHistoryComponent implements OnInit {
    public visitors: VisitorInfo[];
    
    constructor(private statBackendService: StatisticBackendService) {
    }

    ngOnInit(): void {
        this.statBackendService.getVisitors().subscribe(response => {
            this.visitors = response;
        });
    }
}