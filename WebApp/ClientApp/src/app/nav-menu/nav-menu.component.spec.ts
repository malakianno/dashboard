import { NavMenuComponent } from './nav-menu.component'

describe('NavMenuComponent', () => {
    let cmp = new NavMenuComponent();

    it('is not expanded by default', () => {
        expect(cmp.isExpanded).toBeFalse();
    })
});