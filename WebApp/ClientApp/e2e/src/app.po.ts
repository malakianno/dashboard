import { browser, by, element, protractor } from 'protractor';

export class AppPage {
  navigateTo() {
    var EC = protractor.ExpectedConditions;
    var timeout = 15000;
    browser.get('/');
    return browser.wait(EC.presenceOf(element(by.css("weather-forecast-chart"))),timeout)
  }

  getMainHeading() {
    return element(by.css('text.highcharts-title > tspan')).getText();
  }
}
