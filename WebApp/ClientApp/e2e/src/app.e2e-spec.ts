import { AppPage } from './app.po';
import { Client } from '@ng-apimock/base-client';
declare const ngApimock: Client;

describe('App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  it('should display welcome message', async () => {
    await ngApimock.selectScenario('weather-forecast', 'test1');
    page.navigateTo().then(() => {
      page.getMainHeading().then((heading) => {
        expect(heading).toEqual('Weather Forecast');
      });
    });
  });
});
