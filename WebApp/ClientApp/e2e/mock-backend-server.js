const apimock = require('@ng-apimock/core');
const express = require('express');
const path = require('path');
const app = express();

app.set('port', (process.env.PORT || 3000));

// Process the application mocks
apimock.processor.process({
    src: './e2e/mocks'
})
// Use the ng-apimock middelware
app.use(apimock.middleware);

app.listen(app.get('port'), function() {
 console.log('app running on port', app.get('port'));
});