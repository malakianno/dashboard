﻿using Logic.Models.WeatherForecast;
using System.Threading.Tasks;

namespace Logic.Services.Contracts
{
    public interface IWeatherService
    {
        Task<DtoWeatherForecast> GetHourlyWeatherForecast(double latitude, double longitude);
    }
}
