﻿using System.Collections.Generic;
using Logic.Models;

namespace Logic.Services.Contracts
{
    public interface IVisitorsHistoryService
    {
        void Add(VisitorInfo userData);
        IEnumerable<VisitorInfo> Get();
    }
}