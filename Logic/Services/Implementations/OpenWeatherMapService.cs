﻿using Logic.Models.OpenWeatherMap;
using Logic.Models.WeatherForecast;
using Logic.Services.Contracts;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Services.Implementations
{
    public class OpenWeatherMapService : IWeatherService
    {
        private static readonly HttpClient _httpClient = new HttpClient();
        private IConfiguration _configuration;
        private const string WeatherForecastSetting = "WeatherForecast";
        private const string UrlSetting = "url";
        private const string ApiKeySetting = "api-key";
        private const string LatitudeSetting = "lat";
        private const string LongitudeSetting = "lon";

        public OpenWeatherMapService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<DtoWeatherForecast> GetHourlyWeatherForecast(double latitude, double longitude)
        {
            var apiKey = _configuration[$"{WeatherForecastSetting}:{ApiKeySetting}"];
            var urlTemplate = new StringBuilder(_configuration[$"{WeatherForecastSetting}:{UrlSetting}"]);
            var url = urlTemplate
                .Replace($"{{{ApiKeySetting}}}", apiKey)
                .Replace($"{{{LatitudeSetting}}}", latitude.ToString())
                .Replace($"{{{LongitudeSetting}}}", longitude.ToString())
                .ToString();
            using (var response = await _httpClient.GetAsync(url))
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                var forecast = JsonConvert.DeserializeObject<OWPHourlyForecast>(jsonContent);
                return new DtoWeatherForecast()
                {
                    City = forecast.city?.name ?? "N/A",
                    Items = forecast.list.Select(x => {
                        var time = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)
                                .AddSeconds(x.dt)
                                .AddSeconds(forecast.city?.timezone ?? 0);
                        return new DtoWeatherForecastItem()
                        {
                            Time = time,
                            TimeFormatted = time.ToString("dddd HH:mm"),
                            TimeSecondsSince1970 = x.dt,
                            TemperatureC = KelvinToCelsius(x.main.temp, 0),
                            MinTemperatureC = KelvinToCelsius(x.main.temp_min, 0),
                            MaxTemperatureC = KelvinToCelsius(x.main.temp_max, 0),
                            TemperatureF = KelvinToFahrenheit(x.main.temp, 0),
                            MinTemperatureF = KelvinToFahrenheit(x.main.temp_min, 0),
                            MaxTemperatureF = KelvinToFahrenheit(x.main.temp_max, 0),
                            Wind = x.wind?.speed ?? double.NaN,
                            Description = x.weather?.FirstOrDefault()?.description,
                        };
                    })
                };
            }
        }

        private double KelvinToCelsius(double value, int digits)
        {
            return Math.Round(value - 273.15, digits);
        }

        private double KelvinToFahrenheit(double value, int digits)
        {
            return Math.Round((value - 273.15) * 1.8 + 32, digits);
        }
    }
}
