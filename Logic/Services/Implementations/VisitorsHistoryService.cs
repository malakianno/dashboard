using System.Collections.Concurrent;
using System.Collections.Generic;
using Logic.Models;
using Logic.Services.Contracts;

namespace Logic.Services.Implementations
{
    public class VisitorsHistoryService : IVisitorsHistoryService
    {
        private ConcurrentQueue<VisitorInfo> _data = new ConcurrentQueue<VisitorInfo>();
        private const int Capacity = 10000;

        public void Add(VisitorInfo userData)
        {
            if (_data.Count < Capacity)
            {
                _data.Enqueue(userData);
            }
            else
            {
                if (_data.TryDequeue(out VisitorInfo removedUserData))
                {
                    _data.Enqueue(userData);
                }
            }
        }

        public IEnumerable<VisitorInfo> Get()
        {
            using (var enumerator = _data.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    yield return enumerator.Current;
                }
            }
        }
    }
}