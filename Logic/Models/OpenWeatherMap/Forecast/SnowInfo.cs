﻿using Newtonsoft.Json;

namespace Logic.Models.OpenWeatherMap.Forecast
{
    public class SnowInfo
    {
        [JsonProperty(PropertyName = "3h")]
        public double threeHours { get; set; }
    }
}
