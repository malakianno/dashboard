﻿namespace Logic.Models.OpenWeatherMap.Forecast
{
    public class WindInfo
    {
        public double speed { get; set; }
        public double deg { get; set; }
    }
}
