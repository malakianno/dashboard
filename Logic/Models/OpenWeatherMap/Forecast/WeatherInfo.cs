﻿namespace Logic.Models.OpenWeatherMap.Forecast
{
    public class WeatherInfo
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }
}
