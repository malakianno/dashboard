﻿using System.Collections.Generic;

namespace Logic.Models.OpenWeatherMap.Forecast
{
    public class ForecastInfo
    {
        public long dt { get; set; }
        public TemperatureInfo main { get; set; }
        public IEnumerable<WeatherInfo> weather { get; set; }
        public CloudsInfo clouds { get; set; }
        public WindInfo wind { get; set; }
        public RainInfo rain { get; set; }
        public SnowInfo snow { get; set; }
        public SysInfo sys { get; set; }
        public string dt_txt { get; set; }
    }
}
