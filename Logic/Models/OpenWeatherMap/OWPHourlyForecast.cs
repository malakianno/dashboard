﻿using System.Collections.Generic;
using Logic.Models.OpenWeatherMap.Forecast;

namespace Logic.Models.OpenWeatherMap
{
    /// <summary>
    /// Represents hourly weather forecast from openweathermap.org API
    /// </summary>
    public class OWPHourlyForecast
    {
        public string cod { get; set; }
        public double message { get; set; }
        public int cnt { get; set; }
        public IEnumerable<ForecastInfo> list { get; set; }
        public CityInfo city { get; set; }
    }
}
