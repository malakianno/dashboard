﻿namespace Logic.Models.OpenWeatherMap
{
    public class Coordinates
    {
        public double lat { get; set; }
        public double lon { get; set; }
    }
}
