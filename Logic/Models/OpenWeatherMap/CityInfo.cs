﻿namespace Logic.Models.OpenWeatherMap
{
    public class CityInfo
    {
        public int id { get; set; }
        public string name { get; set; }
        public Coordinates coord { get; set; }
        public string country { get; set; }
        public int population { get; set; }
        public long timezone { get; set; }
        public long sunrise { get; set; }
        public long sunset { get; set; }
    }
}
