using System;

namespace Logic.Models
{
    public class VisitorInfo
    {
        public DateTime Timestamp { get; set; }
        public string IpAddress { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string City { get; set; }
    }
}