﻿using System;

namespace Logic.Models.WeatherForecast
{
    public class DtoWeatherForecastItem
    {
        public DateTime Time { get; set; }
        public string TimeFormatted { get; set; }
        public long TimeSecondsSince1970 { get; set; }
        public double TemperatureF { get; set; }
        public double MinTemperatureF { get; set; }
        public double MaxTemperatureF { get; set; }
        public double TemperatureC { get; set; }
        public double MinTemperatureC { get; set; }
        public double MaxTemperatureC { get; set; }
        public double Wind { get; set; }
        public string Description { get; set; }
    }
}
