﻿using System.Collections.Generic;

namespace Logic.Models.WeatherForecast
{
    public class DtoWeatherForecast
    {
        public string City { get; set; }
        public IEnumerable<DtoWeatherForecastItem> Items { get; set; }
    }
}
